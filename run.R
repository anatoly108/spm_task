source("lib.R")

# id 13 is OK
# id 14 is OK
# id 15 is OK 
# id 16 is OK
# id 19 is OK
# id 22 is NOK swich operation abnormal in general

if(!exists("password", inherits=FALSE)){
  password <- rstudioapi::askForPassword("Database password")
}else{
  if (is.null(password))
    password <- rstudioapi::askForPassword("Database password")
}

results.all <- llply(names(switches.periods), function(switch.id){
  print(switch.id)
  end <- as.Date(switches.periods[[switch.id]]$End)
  results <- GetSwitchStatsLoop(switch.id, end)
  list(results=results)
}, .progress="text")
names(results.all) <- names(switches.periods)

results.all.healthy <- llply(names(switches.periods.healthy), function(switch.id){
  end <- as.Date(switches.periods.healthy[[switch.id]]$End)
  results <- GetSwitchStatsLoop(switch.id, end)
  list(results=results)
}, .progress="text")
names(results.all.healthy) <- names(switches.periods.healthy)
results.all.healthy$`14`$results$anomalies.plot
saveRDS(results.all.healthy, "results.all.healthy2july23.rds")


switch.id <- "13"
end <- as.Date("2019-07-24")
results <- GetSwitchStatsLoop(switch.id, end, password=password)
